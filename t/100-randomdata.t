use Test::More;

use Zaaksysteem::Tools::RandomData qw(:all);
use Zaaksysteem::Tools qw(elfproef);


my $bsn = generate_bsn();
ok(elfproef($bsn, 1), "generate_bsn ($bsn) is BSN elfproef");

my $rsin = generate_rsin();
ok(elfproef($rsin), "generate_rsin ($rsin) is elfproef");

my $kvk = generate_kvk();
ok(elfproef($kvk), "generate_kvk ($kvk) is elfproef");

done_testing;
