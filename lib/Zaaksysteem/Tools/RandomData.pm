package Zaaksysteem::Tools::RandomData;
use warnings;
use strict;

=head1 NAME

Zaaksysteem::Tools::RandomData - Tools for generating fake data

=head1 DESCRIPTION

This module provides methods to generate fake and/or random data used
for spoofing and/or faking data such as BSN numbers and KvK numbers.

=head1 SYNOPSIS

    use Zaaksysteem::Tools::RandomData qw(generate_bsn);

    my $fake_bsn  = generate_bsn();
    my $fake_kvk  = generate_kvk();
    my $fake_rsin = generate_rsin();

=cut

use autodie;

use feature ();
use Exporter qw(import);

our @EXPORT_OK = (
    qw(
        generate_bsn
        generate_rsin
        generate_kvk
    ),
);

our %EXPORT_TAGS = (
    all => \@EXPORT_OK
);

=head2 generate_bsn

Generate a BSN

=cut

sub generate_bsn {
    my @bsn;
    while(@bsn < 9) {
        push(@bsn, int(rand(10)));
    }

    # BSN mag niet met 00 beginnen
    while ($bsn[0] == 0 && $bsn[1] == 0) {
        $bsn[0] = int(rand(10));
        $bsn[1] = int(rand(10));
    }

    @bsn = reverse(@bsn);

    my $sum = 0;
    foreach my $i (reverse(1..8)) {
        $sum += (($i + 1) * $bsn[$i]);
    }

    my $last_number = $sum % 11;
    # if the last number is 10, we have an invalid number
    return generate_bsn() if $last_number > 9;

    @bsn = reverse(@bsn);

    $bsn[-1] = $last_number;
    return join("", @bsn);
}


sub _get_last_number {
    my @set = @_;

    @set = reverse(@set);

    my $sum = 0;
    foreach my $i (reverse(1..8)) {
        $sum += (($i + 1) * $set[$i]);
    }

    my $left = $sum % 11;
    my $last_number = abs($left - 11);

    # if the last number is 10, we have an invalid number
    return if $last_number > 9;

    $set[0] = $last_number;
    return reverse(@set);
}

=head2 generate_kvk

Generate a KvK number

=cut

sub generate_kvk {
    my @kvk;
    push(@kvk, 0); # first number is 0
    while(@kvk < 9) {
        push(@kvk, int(rand(10)));
    }

    @kvk = _get_last_number(@kvk);
    if (@kvk) {
        shift @kvk;
        return join("", @kvk);
    }
    return generate_kvk();
}

=head2 generate_rsin

Generate a RSIN number

=cut

sub generate_rsin {
    my @rsin;

    while(@rsin < 9) {
        push(@rsin, int(rand(10)));
    }

    @rsin = _get_last_number(@rsin);
    return join("", @rsin) if @rsin;
    return generate_rsin();
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
